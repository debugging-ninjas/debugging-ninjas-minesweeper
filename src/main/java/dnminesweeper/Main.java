package dnminesweeper;

import javax.swing.*;
import java.awt.*;

/** Our main class that launches the app. */
public class Main {
    
    public static void main(String... args) throws Exception {

        JFrame mainWindow = new JFrame("Debugging Ninjas Minesweeper");

        mainWindow.pack();
        mainWindow.setVisible(true);

        // This sets what to do when we close the main window.
        mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}
